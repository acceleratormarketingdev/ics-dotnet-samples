import React, { Component } from 'react';
import {connect} from 'react-redux';

class App extends Component {

  componentWillMount(){
    this.makeLDSRequest();
  }

  componentDidMount(){
    console.log(this.props);
  }

  makeLDSRequest(){
    var data = JSON.stringify({"someparam": "someval"});
        
    var xhr = new XMLHttpRequest();
        
    xhr.addEventListener("readystatechange", ()=>{
        if (this.readyState === 4) {
            console.log(this.responseText);
            var parsedLearnosityDS = JSON.parse(this.responseText);
            console.log(parsedLearnosityDS);
            window.LearnosityItems.init(parsedLearnosityDS);
            if(window.LearnosityItems.error!=undefined){
              console.log(window.LearnosityItems.error[0]);
            }
        }
    });
        
    xhr.open("POST", "http://localhost:8000/");
    xhr.setRequestHeader("Content-Type", "application/json");
    //xhr.setRequestHeader("Cache-Control", "no-cache");
        
    xhr.send(data);
  }

  render() {
    return (
      <div>
        <span>Assessment</span>
        <div id="learnosity_assess"></div>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      appReducer:state
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      updateSomeParam:(payloadObj)=>{
          dispatch({
              type:"UpdateSomeParam",
              payload:payloadObj
          });
      }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(App);