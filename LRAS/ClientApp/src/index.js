import './index.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import App from './App';
import appReducer from './store/reducers/appreducer.js';

const appstore = createStore(appReducer, {someparam:'asdasd'});
const rootElement = document.getElementById('root');

ReactDOM.render(<Provider store={appstore}><App /></Provider>,rootElement);
