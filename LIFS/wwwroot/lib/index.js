window.onload = function(){
    console.log("JS Loaded");
    makeLDSRequest();
}

function makeLDSRequest(){
    var data = JSON.stringify({"someparam": "someval"});
        
    var xhr = new XMLHttpRequest();
        
    xhr.addEventListener("readystatechange", ()=>{
        if (xhr.readyState === 4) {
            //console.log(this.responseText);
            var parsedLearnosityDS = JSON.parse(xhr.responseText);
            console.log(parsedLearnosityDS);
            var itemApp = window.LearnosityItems.init(parsedLearnosityDS,

              {
                readyListener: () => {
                  var questions = itemApp.questions();
                  var items = itemApp.getItems();
                  var tags = itemApp.getTags(true);
          
                  for (let item in tags) {
                    items[item].response_ids.forEach((question) => {
                      questions[question].tags = tags[item];
                    });
                  }
          
                  Object.values(questions).forEach(question => {
                    question.on('validated', () => {
                      question.disable();
          
                      var passage = question.tags.Passage[0];
          
                      onCheckAnswerClicked(question);
          
                      var passageComplete = !Object.values(questions).find((q) => {
                        return q.tags.Passage[0] === passage && q.getScore().score === null
                      });
                      if (passageComplete) {
                        var passageQuestions = Object.values(questions).filter((q) => {
                          return q.tags.Passage[0] == passage;
                        });
                        onPassageComplete(passageQuestions);
                      }
          
                      var unitComplete = !Object.values(questions).find((q) => {
                        return q.getScore().score === null;
                      });
                      if (unitComplete) {
                        onUnitComplete(Object.values(questions));
                      }
          
                    });
                  });
                }
              }

            );
        }
    });
        
    xhr.open("POST", "http://localhost:8000/");
    xhr.setRequestHeader("Content-Type", "application/json");
    //xhr.setRequestHeader("Cache-Control", "no-cache");
        
    xhr.send(data);
  }

  function onCheckAnswerClicked(question){
    var response = question.getResponse();
    var metadata = question.getMetadata();
    var feedback = response.value.map((value) => {
      return metadata.distractor_rationale_response_level[value];
    });
    var reference = metadata.sheet_reference;
    var isCorrect = question.isValid()
  
    console.log('answered question', reference, isCorrect ? 'correcly' : 'incorrectly');
    console.log('display feedback', feedback);
  }
  
  function onPassageComplete(questions) {
    var negativeFeedbackCanDos = [];
    questions.forEach((question) => {
      if (!question.isValid()) {
        negativeFeedbackCanDos.push(question.tags.CanDo[0]);
      }
    });
  
    console.log('completed passage', questions[0].tags.Passage, 'display negative feedback for', negativeFeedbackCanDos);
  }
  
  function onUnitComplete(questions) {
    var missedCanDoCount = {};
  
    questions.forEach((question) => {
      if (!question.isValid()) {
        var canDo = question.tags.CanDo[0];
        missedCanDoCount[canDo] = missedCanDoCount[canDo] || {'canDo': canDo, 'count': 0};
        missedCanDoCount[canDo].count++;
      }
    });
  
    var sorted = Object.values(missedCanDoCount);
    sorted.sort((a, b) => {
      a.count - b.count;
    });
  
    console.log('completed unit');
    console.log('display positive feedback for', sorted[sorted.length - 1]);
    console.log('display negative feedback for', sorted[0]);
  }