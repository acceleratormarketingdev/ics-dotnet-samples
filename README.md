
This Repo Contains 2 projects, Learnosity Intermediary Backend Service(LIBS) and Learnosity Frontend Service. To run these projects,

For MacOS:

1. Make sure "dotnet" is installed. Download: https://www.microsoft.com/net/download/macos

2. Go to LIBS/LIFS project folders, each at a time and build project using

	#dotnet build

then run using,

	#dotnet run

3. LIBS runs on port 8000 on localhost / LIFS runs on port 5000 on localhost


[WIP]To create projects from scratch:

1. Create a new empty console project

	#dotnet new console

2. copy over C# LearnositySDK project into generated project folder. Add package reference into current project
	dotnet add reference <Learnosity sdk project path>

	e.g.

	dotnet add reference lib/LearnositySDK/LearnositySDK.csproj

C# Learnosity SDK also requires Newtonsoft JSON package as a dependency, add

	dotnet add package Newtonsoft.Json

3. [optional]create Learnosity Intermediary Controller to manage SDK

4. [optional]For DB Communication, create a DB Connector Controller Class then add MySQL DB Connector package using

	dotnet add package MySql.Data
	
[Need to optimize mysql connector to be able to pool connection, needs more research]
