﻿using System;

//LIBS: Learnosity Intermediary Backend Service
namespace LIBS
{
    class Program
    {

        DBController dbcObj;
        LIBSHttpServer libshsObj;
        ScrapperUtil suObj;

        public Program(){
            dbcObj = new DBController();
            libshsObj = new LIBSHttpServer();
            suObj = new ScrapperUtil();
        }

        static void Main(string[] args)
        {
            Program mainProg = new Program();
            Console.WriteLine("Learnosity Intermediary Backend Service Started...");
            mainProg.libshsObj.startService();
        }
    }
}
