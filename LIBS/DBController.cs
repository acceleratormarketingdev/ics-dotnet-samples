using System;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace LIBS
{
    class DBController{

        private string connStr = "server=10.1.10.85;user=yogi;database=test;port=3306;password=password;SSL Mode=none";
        private MySqlConnection conn;

        public DBController(){
            this.conn = new MySqlConnection(connStr);
        }

        public void querySelect(){
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                this.conn.Open();

                string sql = "SELECT id, somefield1, somefield2 FROM sampletable";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Console.WriteLine(rdr[0]+" -- "+rdr[1]+" -- "+rdr[2]);
                }
                rdr.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            this.conn.Close();
            Console.WriteLine("Done.");
        }
    }
}