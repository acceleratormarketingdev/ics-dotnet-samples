using System;
using LearnositySDK;
using LearnositySDK.Utils;
using LearnositySDK.Request;

namespace LIBS
{
    class LearnosityController{

        public string makeSDKRequest(){
            Console.WriteLine("Making Learnosity SDK Request");

            string service = "items";

            JsonObject security = new JsonObject();
            security.set("consumer_key", "nG7kmX71gq21ZKka");
            security.set("domain", "localhost");
            security.set("user_id", "demo_user");

            string secret = "mfs9wsTeacYF6F6uWEeIllKScMUBgEmTtfcqX6V7";

            JsonObject request = new JsonObject();
            request.set("rendering_type", "inline");
            request.set("retrieve_tags", true);
            request.set("activity_template_id", "c5f63c30-96f0-40e4-a0dd-9b08b2763abf");
            request.set("activity_id", "c5f63c30-96f0-40e4-a0dd-9b08b2763abf");
            //request.set("name", "Reading 1 - Practice");;
            request.set("session_id", Uuid.generate());
            request.set("type", "local_practice");
            request.set("user_id", "demo_user");

            // Instantiate Init class
            Init init = new Init(service, security, secret, request);

            // Call the generate() method to retrieve a JavaScript object
            string JavaScriptObject = init.generate();

            Console.WriteLine(JavaScriptObject);

            return JavaScriptObject;
        }
    }
}