using System;
using System.Net;
using System.IO;
using System.Threading;

namespace LIBS
{
    class LIBSHttpServer{

        static HttpListener webListener;
        private Thread listenerThread;

        LearnosityController lcObj;

        public LIBSHttpServer(){
            lcObj = new LearnosityController();
        }

        public void startService(){
            webListener = new HttpListener();
            webListener.Prefixes.Add("http://localhost:8000/");
            webListener.Prefixes.Add("http://127.0.0.1:8000/");
            webListener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;        //I don't hink this line is needed, I need to do more research on that

            webListener.Start();
            this.listenerThread = new Thread(new ParameterizedThreadStart(startListener));
            listenerThread.Start();
            Console.WriteLine("LIBS Request Listening On Port 8000");
        }

        private void startListener(object s){
            while (true)
            {
                ////blocks until a client has connected to the server
                processRequest();
            }
        }

        private void processRequest(){
            //var result = webListener.BeginGetContext(listenerCallback, webListener);
            IAsyncResult result = webListener.BeginGetContext(listenerCallback, webListener);
            result.AsyncWaitHandle.WaitOne();
        }

        private void listenerCallback(IAsyncResult result){
            HttpListenerContext context = webListener.EndGetContext(result);
            Thread.Sleep(1000);
            string data_text = new StreamReader(context.Request.InputStream, context.Request.ContentEncoding).ReadToEnd();

            /*functions used to decode json encoded data.
            JavaScriptSerializer js = new JavaScriptSerializer();
            var data1 = Uri.UnescapeDataString(data_text);
            string da = Regex.Unescape(data_text);
            var unserialized = js.Deserialize(data_text, typeof(String));*/

            string cleaned_data = System.Web.HttpUtility.UrlDecode(data_text);
            Console.WriteLine(cleaned_data);

            context.Response.StatusCode = 200;
            context.Response.StatusDescription = "OK";

            if (context.Request.HttpMethod == "OPTIONS"){
                context.Response.AddHeader("Access-Control-Allow-Headers", "*");
            }
            context.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type");
            context.Response.AddHeader("Access-Control-Allow-Origin", "*");
            context.Response.AddHeader("Content-Type", "application/json");

            /*use this line to get your custom header data in the request.
            var headerText = context.Request.Headers["mycustomHeader"];
            use this line to send your response in a custom header
            context.Response.Headers["mycustomResponseHeader"] = "mycustomResponse";*/

            string learnositySDKResponse = lcObj.makeSDKRequest();

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(learnositySDKResponse);
            // Get a response stream and write the response to it.
            context.Response.ContentLength64 = buffer.Length;
            Stream output = context.Response.OutputStream;
            output.Write(buffer,0,buffer.Length);
            output.Close();

            context.Response.Close();
        }

    }
}