using System;
using System.Net;
using System.IO;

namespace LIBS
{
    class ScrapperUtil{
        public void scrapeDS(){
            WebClient _client = new WebClient();

            _client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

            Stream scrappedDS = _client.OpenRead("http://quotes.toscrape.com/");
            StreamReader DSreader = new StreamReader(scrappedDS);
            string parsedString = DSreader.ReadToEnd();
            scrappedDS.Close();
            DSreader.Close();

            Console.WriteLine(parsedString);
            //return parsedString;
        }
    }
}